
// 1. Опишіть своїми словами, що таке метод об'єкту.
// Це функція вбудована в об'єкт, призначена для виконання певних дій з його властивостями.
// 2. Який тип даних може мати значення властивості об'єкта?
// Значення можуть бути будь якого типу.
// 3.Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає що обєкт бере свої значення або властивості у іншого обєкта (тобто посилається на нього) 



function createNewUser () {

    const newUser = {
        firstName : prompt("Введіть своє ім'я?"),
        lastName : prompt ("Введіть своє призвіще?"),
        getLogin (){
            return (this.firstName[0] + this.lastName).
            toLowerCase();
        }
    }
    Object.defineProperties(newUser, {
    firstName: {writable: false},
    lastName: {writable: false},
    });
  
    return newUser;
}


let userOne = createNewUser();
console.log(userOne.getLogin());
console.log(userOne);
userOne.firstName = 'Vitya';
console.log(userOne);
